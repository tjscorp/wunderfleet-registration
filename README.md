## Setup

This project was done in a PHP framework called laravel due to its MVC structure, allowing scalability and speedy development.
To set it up, you will have to perform the following few step

**WINDOWS SETUP**
1. Install XAMP on windows https://www.apachefriends.org/download.html
2. Clone this repo into **C:\xampp\htdocs**.
3. Open the XAMP control panel and start Apache and MySQL.
4. Open your web browser and navigate to http://localhost/phpmyadmin
5. Create a new database called **wunderfleet**.
6. Import the **wunderfleet.sql** file provided with the project into the **wunderfleet** database that you just created.
7. In your web browser, navigate to http://localhost/wunderfleet-registration


**LINUX SETUP**
1. Install the LAMP stack. https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-16-04
2. Install phpMyAdmin https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-phpmyadmin-on-ubuntu-16-04
3. Clone this repo into **/var/www/html/** for Apache on Linux.
4. Open your web browser and navigate to http://localhost/phpmyadmin
5. Create a new database called **wunderfleet**.
6. Import the **wunderfleet.sql** file provided with the project into the **wunderfleet** database that you just created.
7. In your web browser, navigate to http://localhost/wunderfleet-registration

---


## Performance Enhancements

1. Add a virtual host in Apache config to shorten urls and avoid unnecessary redirects.
2. Limit the use of third party plugins, such as the validate.js
3. Use Eloquents eager loading of data
4. Prefer using local js rather than using CDNs depending on network speed
5. Limit included libraries
6. Cache routes

---


## Things to improve on

1. Add unit tests all around the project, especially test the Registration controller save method to make sure no unexpected bugs arise.
2. Add try catch blocks when saving the data into the database to catch expections before they cause crashes.
3. Add a virtual host in Apache config to shorten urls and avoid unnecessary redirects
4. Use a front-end framework with better tools and built packages such as Angular for convinience and speedier development rather than re-writing functions. eg: [patchForm()]
5. Use specific routes, rather than resource routes where some methods are not implemented