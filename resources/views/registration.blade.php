@extends('layouts.base')

@section('content')

<h1 class="text-center">Hi, We at Wunderfleet would like to get to know you!</h1>     
    
    {!! Form::open(array('action'=>'RegistrationController@store', 'method'=>'POST', 'id'=>'reg-form', 'style'=>'max-width: 600px;', 'class'=>'mx-auto my-5 clearfix', 'novalidate'=>true)) !!}

        <div id="personal-info">
            <!-- SECTION 1: PERSONAL INFO -->
            <h2 class="text-center">Care to tell us a little about yourself?</h2>
            <div class="form-group">
                {{ Form::label('firstname', 'First Name') }}
                {{ Form::text('firstname', null, array('class'=>'form-control', 'placeholder' => 'What is your first name?', 'required'=>true)) }}
                <div class="invalid-feedback">Oopsy, you can't leave this empty.</div>
            </div>
            <div class="form-group">
                {{ Form::label('lastname', 'Last Name') }}
                {{ Form::text('lastname', null, array('class'=>'form-control', 'placeholder' => 'Have a last name?', 'required'=>true)) }}
                <div class="invalid-feedback">Oopsy, you can't leave this empty.</div>
            </div>
            <div class="form-group">
                {{ Form::label('telephone', 'Telephone') }}
                {{ Form::tel('telephone', null, array('class'=>'form-control', 'placeholder' => 'What number could we call you on?', 'required'=>true)) }}
                <div class="invalid-feedback">Oopsy, you can't leave this empty.</div>
            </div>
            <div class="form-group">
                <button onclick="showStep('address-info')" type="button" class="btn btn-outline-secondary float-right"> Next > </button>
            </div>
        </div>

        <div id="address-info" style="display: none;">
            <!-- SECTION 2: ADDRESS INFO -->
            <h2 class="text-center">Where do you stay?, we promise not to stalk!</h2>
            <div class="form-group">
                {{ Form::label('street', 'Street') }}
                {{ Form::text('street', null, array('class'=>'form-control', 'placeholder' => 'Your street name?', 'required'=>true)) }}
                <div class="invalid-feedback">Oopsy, you can't leave this empty.</div>
            </div>
            <div class="form-group">
                {{ Form::label('house_number', 'House Number') }}
                {{ Form::text('house_number', null, array('class'=>'form-control', 'placeholder' => 'What is your house number?', 'required'=>true)) }}
                <div class="invalid-feedback">Oopsy, you can't leave this empty.</div>
            </div>
            <div class="form-group">
                {{ Form::label('zip_code', 'Zip Code') }}
                {{ Form::text('zip_code', null, array('class'=>'form-control', 'placeholder' => 'Do you have a zip code?', 'required'=>true)) }}
                <div class="invalid-feedback">Oopsy, you can't leave this empty.</div>
            </div>
            <div class="form-group">
                {{ Form::label('city', 'City') }}
                {{ Form::text('city', null, array('class'=>'form-control', 'placeholder' => 'Which city are you from?', 'required'=>true)) }}
                <div class="invalid-feedback">Oopsy, you can't leave this empty.</div>
            </div>
            <div class="form-group">
                <button onclick="showStep('personal-info')" type="button" class="btn btn-outline-secondary float-left"> < Previous </button>
                <button onclick="showStep('payment-info')" type="button" class="btn btn-outline-secondary float-right">Next > </button>
            </div>
        </div>
    
        <div id="payment-info" style="display: none;">
            <!-- SECTION 3: PAYMENT INFO -->
            <h2 class="text-center">How would you like to pay?</h2>
            <div class="form-group">
                {{ Form::label('iban', 'IBAN') }}
                {{ Form::text('iban', null, array('class'=>'form-control', 'placeholder' => 'We just need your IBAN number?', 'required'=>true)) }}
                <div class="invalid-feedback">Oopsy, you can't leave this empty.</div>
            </div>
            <div class="form-group">
                <button onclick="showStep('address-info')" type="button" class="btn btn-outline-secondary float-left"> < Previous </button>
                {{ Form::submit('Finish', array('class'=>'btn btn-outline-dark float-right')) }}
            </div>
        </div>

    {!! Form::close() !!}
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/mdb.min.js') }}"></script>
    <script src="{{ asset('js/HoldOn.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/registration.js') }}"></script>
@endsection
