<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>{{ config('app.name') }}</title>
  <meta name="author" content="Jayden Takudzwanashe Shamhu">
  <meta name="description" content="Wunderfleet test assignment">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="{{ asset('css/app.css') }}"/>
<link rel="stylesheet" href="{{ asset('css/HoldOn.min.css') }}">

</head>

<body>
  @include('layouts.navbar')
  <div class="container">
      @yield('content')
  </div>
    
</body>

</html>