@extends('layouts.base')

@section('content')

<h1 class="text-center">Congratulations, you're Wunderfleet's newset member!</h1>     

<h2 class="text-center">Here is your payment data ID:</h2>
<h4 class="text-center my-5" style="word-break: break-word;">
    {{ $paymentDataId }}
</h4>

@endsection