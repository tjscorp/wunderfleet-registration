<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentInformation extends Model
{
    protected $table = 'payment_information';
}
