<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\PaymentInformation;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;


class RegistrationController extends Controller
{
    public function index() {
        return view('registration');
    }

    public function store(Request $request)
    {
        // create user
        $user = new User();
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->telephone = $request->telephone;
        $user->street = $request->street;
        $user->house_number = $request->house_number;
        $user->zip_code = $request->zip_code;
        $user->city = $request->city;
        $user->save();

        // get the payment data id
        $client = new Client();
        try {
            $res = $client->post(
                'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data', [
                'body' => json_encode(
                    array(
                        'customerId' => $user->id,
                        'iban' => $request->iban,
                        'owner' => $request->firstname . ' ' . $request->lastname
                        )
                    )
            ]);  
        } catch (ClientException $e) {
            $response = $e->getResponse();
            $responseBody = $response->getBody()->getContents();
            $responseCode = $response->getStatusCode();
            $user->delete();
            return response($responseBody, $responseCode)->header('Content-Type', 'application/json');
        }

        $jsonReponse = json_decode($res->getBody(), true);

        // create payment information
        $paymentInformation = new PaymentInformation();
        $paymentInformation->account_owner = $user->id;
        $paymentInformation->iban = $request->iban;
        $paymentInformation->payment_data_id = $jsonReponse['paymentDataId'];
        $paymentInformation->save();
        
        session()->put('paymentDataId', $jsonReponse['paymentDataId']);
        return response($res->getBody(), $res->getStatusCode())->header('Content-Type', 'application/json');
    }

    public function success() {
        $paymentDataId = session()->get('paymentDataId');
        if (!$paymentDataId){
            return redirect()->action('RegistrationController@index');
        }
        return view('success')->with('paymentDataId', $paymentDataId);
    }
}
