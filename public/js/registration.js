$("#reg-form").submit(function (e) {
    e.preventDefault();
    $(this).addClass('was-validated');

    if (jumpToFirstError($(this)))
        return

    const CSRF_TOKEN = $("input[name='_token']").val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': CSRF_TOKEN
        }
    });
    const data = {
        _token: CSRF_TOKEN,
        ...formToJSON($(this))
    };
    HoldOn.open();
    $.ajax({
        url: "/registration",
        type: 'POST',
        data: data,
        dataType: 'JSON',
        timeout: 30000,
        success: function (data) {
            localStorage.removeItem('cachedFormData');
            localStorage.removeItem('lastStepId');
            window.location = '/success';
            HoldOn.close();
        },
        error: function (error) {
            console.log(error);
            HoldOn.close();
        }
    });
});

function formToJSON(form) {
    let formArray = form.serializeArray();
    let returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}

function patchForm(form, data) {
    for (let key in data) {
        if (data.hasOwnProperty(key)) {
            form.children().find(`input[name='${key}']`).val(data[key])
        }
    }
}

$("#reg-form").find("input").on('input', function () {
    localStorage.setItem('cachedFormData', JSON.stringify(formToJSON($("#reg-form"))));
});

$(document).ready(function () {
    let cachedFormData = localStorage.getItem('cachedFormData');
    if (!cachedFormData)
        return;
    cachedFormData = JSON.parse(cachedFormData);
    delete cachedFormData['_token'];
    patchForm($("#reg-form"), cachedFormData);
    loadLastStep();
});

function loadLastStep() {
    let lastStepId = localStorage.getItem('lastStepid');
    if (!lastStepId)
        return;
    showStep(lastStepId);
}

function showStep(stepId) {
    localStorage.setItem('lastStepid', stepId);
    $(`#${stepId}`).siblings("div").fadeOut(500).promise().done(function () {
        $(`#${stepId}`).fadeIn(500);
    });
}

function jumpToFirstError(form){
    let formData = formToJSON(form);
    for (let key in formData) {
        if (formData.hasOwnProperty(key) && !formData[key]) {
            const formControl = $(`input[name='${key}']`);
            const containerId = formControl.parent().parent().attr('id');
            showStep(containerId);
            return true;
        }
    }
    return false;
}